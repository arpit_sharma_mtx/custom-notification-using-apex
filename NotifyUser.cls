public with sharing class NotifyUser{
    /*
     * Params: - Set<String> recipientsIds -> includes User,Group,Queue,Account,Contact 
               - String targetId           -> user will navigate to this record when clicks on notification.          
     * Purpose: create and send custom notification
    */
    public static void notifyUsers(Set<String> recipientsIds, String targetId) {
 
        // Get the Id for our custom notification type
        CustomNotificationType notificationType = 
            [SELECT Id, DeveloperName 
             FROM CustomNotificationType 
             WHERE DeveloperName='Your_Custom_Notification'];
        
        // Create a new custom notification
        Messaging.CustomNotification notification = new Messaging.CustomNotification();
 
        // Set the contents for the notification
        notification.setTitle('Knowledge Article Published');
        notification.setBody('The Knowledge article is published');
 
        // Set the notification type and target
        notification.setNotificationTypeId(notificationType.Id);
        notification.setTargetId(targetId);
        
        // Actually send the notification
        try {
            notification.send(recipientsIds);
        }
        catch (Exception e) {
            System.debug('Problem sending notification: ' + e.getMessage());
        }
    }
}